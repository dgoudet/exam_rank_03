#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

typedef struct	s_zone
{
	int width;
	int height;
	char c;
	char *drawing;
}		t_zone;

typedef struct	s_cir
{
	char type;
	float x;
	float y;
	float radius;
	char c;
}		t_cir;

int	ft_strlen(char *s)
{
	int i = 0;

	while (s[i])
		i++;
	return (i);
}

char	*ft_strdup(char *s)
{
	char *s2;
	int i = 0;

	if (!(s2 = malloc(sizeof(char) * (ft_strlen(s) + 1))))
		return (0);
	while (s[i])
	{
		s2[i] = s[i];
		i++;
	}
	s2[i] = '\0';
	return (s2);
}

void	draw_zone(t_zone *zone)
{
	int x = 0;
	int y = 0;
	int i = 0;

	if (!(zone->drawing = malloc(sizeof(char) * (zone->height * zone->width + zone->height + 1))))
		return ;
	while(y < zone->height)
	{
		while (x < zone->width)
		{
			zone->drawing[i] = zone->c;
			x++;
			i++;
		}
		zone->drawing[i] = '\n';
		i++;
		x = 0;
		y++;
	}
	zone->drawing[i] = '\0';
}

void	print(char *drawing)
{
	int i = 0;

	while (drawing[i])
	{
		write(1, &drawing[i], 1);
		i++;
	}
}

int	is_cir(t_cir *cir, float x, float y)
{
	float distance;

	/*dist = sqrtf(((cir->x - x) * (cir->x - x)) + ((cir->y - y) * (cir->y - y)));
	//printf("dist = %d\n", dist);
	if (((dist - cir->radius) < 1.0000000) && ((dist - cir->radius) >= 0.0000000))
		return (1);
	if (((dist - cir->radius) < 0) && (cir->type == 'C'))
		return (1);
	return (0);*/
	distance = sqrtf(powf(x - cir->x, 2.) + powf(y - cir->y, 2.));
	if (distance <= cir->radius)
	{
		if ((cir->radius - distance) < 1.00000000 || cir->type == 'C')
			return (1);
	}
	return (0);
}

void	draw_cir(t_cir *cir, t_zone *zone)
{
	char *tmp;
	int i = 0;
	float x = 0;
	float y = 0;

	tmp = ft_strdup(zone->drawing);
	free(zone->drawing);
	if (!(zone->drawing = malloc(sizeof(char) * (ft_strlen(tmp) + 1))))
		return ;
	while (tmp[i])
	{
		if (tmp[i] == '\n')
		{
			zone->drawing[i] = tmp[i];
			i++;
			x = 0;
			y++;
		}
		else if (is_cir(cir, x, y) == 1)
		{
			zone->drawing[i] = cir->c;
			i++;
			x++;
		}
		else
		{
			zone->drawing[i] = tmp[i];
			i++;
			x++;
		}
	}
	zone->drawing[i] = '\0';
	free(tmp);
}

int	main(int argc, char **argv)
{
	t_zone	*zone;
	t_cir	*cir;
	FILE *file;
	int ret;

	if (argc != 2)
	{
		write(1, "Error: argument\n", 16);
		return (1);
	}
	if (!(file = fopen(argv[1], "r")))
	{
		write(1, "Error: Operation file corrupted\n", 32);
		return (1);
	}
	if (!(zone = malloc(sizeof(t_zone))) || !(cir = malloc(sizeof(t_cir))))
		return (1);
	if ((fscanf(file, "%d %d %c\n", &zone->width, &zone->height, &zone->c)) != 3)
	{
		write(1, "Error: Operation file corrupted\n", 32);
		return (1);
	}
	if ((zone->width < 1) || (zone->width > 300) || (zone->height < 1) || (zone->height > 300))
	{
		write(1, "Error: Operation file corrupted\n", 32);
		return (1);
	}
	draw_zone(zone);
	while ((ret = fscanf(file, "%c %f %f %f %c\n", &cir->type, &cir->x, &cir->y, &cir->radius, &cir->c)) == 5)
	{
		if (cir->radius <= 0 || (cir->type != 'C' && cir->type != 'c'))
		{
			write(1, "Error: Operation file corrupted\n", 32);
			return (1);
		}
		draw_cir(cir, zone);
	}
	if (ret == -1)
	{
		print(zone->drawing);
		return (0);
	}
	write(1, "Error: Operation file corrupted\n", 32);
	return (1);
}
